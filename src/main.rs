
#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

use rocket::http::ContentType;
use rocket::response::{ self, Response, Responder };
use rocket::request::Request;
use rocket::http::hyper::header::AccessControlAllowOrigin;

use boolean_network::*;

static mut SERVER_STATE: ServerState = ServerState::new();

struct ServerState {
    is_busy: bool,
    model: Option<BooleanNetwork>,
    result: Option<ComponentsResult>, 
}

impl ServerState {
    pub const fn new() -> Self {
        ServerState { is_busy: false, model: None, result: None }
    }

    pub fn is_busy(&self) -> bool {
        self.is_busy
    }

    pub fn set_model(&mut self, model: &BooleanNetwork) {
        self.model = Some(model.clone());
        self.result = None;
    }

    pub fn get_result(&mut self) -> Option<&ComponentsResult> {
        if let None = self.result {
            self.compute()
        }

        self.result.as_ref()
    }

    pub fn get_model(&self) -> Option<&BooleanNetwork> {
        self.model.as_ref()
    }

    fn set_busy(&mut self, new: bool) {
        self.is_busy = new;
    }

    fn compute(&mut self) {
        if let None = self.model {
            return
        }

        self.set_busy(true);
        let model = self.model.as_ref().unwrap();
        self.result = Some(model.state_space().terminal_components());
        self.set_busy(false);
    }
}



struct BackendResponse {
    message: String
}

impl BackendResponse {
    fn new(message: &String) -> Self {
        BackendResponse { message: message.clone() }
    }
}

impl<'r> Responder<'r> for BackendResponse {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        use std::io::Cursor;

        let cursor = Cursor::new(self.message);
        Response::build()
                 .header(ContentType::Plain)
                 .header(AccessControlAllowOrigin::Any)
                 .sized_body(cursor)
                 .ok()
    }
}



#[get("/get_info")]
fn get_info() -> BackendResponse {
    unsafe {
        let (ru, mo, re) = (SERVER_STATE.is_busy, 
                            SERVER_STATE.model.is_some(),
                            SERVER_STATE.result.is_some());

        BackendResponse::new(&format!("{{\"busy\": {}, \"has_model\": {}, \"has_result\": {}}}", ru, mo, re))
    }
}

#[get("/get_result")]
fn get_result() -> BackendResponse {
    let vts = |v: &Vec<BehavioralClass>| v.iter().map(|e| format!("{:?}", e)).collect::<Vec<_>>();

    let result = unsafe { SERVER_STATE.get_result() };
    if let None = result {
        return BackendResponse::new(&String::from("None"))
    }

    let lines = result.unwrap().result_vector().iter()
        .map(|(f, p)| format!("{{\"sat_count\":{},\"phenotype\":{:?}}}", p.sat_count(), vts(&f)))
        .collect::<Vec<_>>();

    let mut json = String::new();
    for index in 0..lines.len() - 1 {
        json += &format!("{},", lines[index]);
    }
    json = format!("{{\"result\":[{}{}]}}", json, lines.last().unwrap());

    BackendResponse::new(&json)
}

#[get("/get_model")]
fn get_model() -> BackendResponse {
    unsafe {
        BackendResponse::new(&match SERVER_STATE.get_model() {
            None => String::from("None"),
            Some(model) => model.to_string()
        })
    }
}

#[post("/sbml_to_aeon", data = "<data>")]
fn sbml_to_aeon(data: String) -> BackendResponse {

   println!("data was {}", data);

   let boolnet = BooleanNetwork::from_sbml(&data);
   match boolnet {
        Ok(result) => BackendResponse::new(&String::from(result.to_string())),
        _ => BackendResponse::new(&String::from("None"))
   }
}

#[get("/set_model/<boolnet>")]
fn set_model(boolnet: String) -> BackendResponse {
    use std::str::FromStr;

    unsafe {
        if SERVER_STATE.is_busy() {
            BackendResponse::new(&String::from("server is busy."))
        } else {
            SERVER_STATE.set_model(&BooleanNetwork::from_str(&boolnet).unwrap());
            BackendResponse::new(&String::from("Ok"))
        }
    }  
}

fn main() {
    //rocket::ignite().mount("/", routes![get_info, get_model, get_result, set_model, sbml_to_aeon]).launch();
    
    println!("{}", BooleanNetwork::from_sbml("asdjasd").unwrap());

}
